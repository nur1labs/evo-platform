#include "BlockchainUtils.h"

namespace CryptoNote {
namespace Utils {
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool restoreCachedTransactions(const std::vector<BinaryArray>& binaryTransactions,
                               std::vector<CachedTransaction>& transactions) {
  transactions.reserve(binaryTransactions.size());

  for (auto binaryTransaction : binaryTransactions) {
    Transaction transaction;
    if (!fromBinaryArray(transaction, binaryTransaction)) {
      return false;
    }

    transactions.emplace_back(std::move(transaction));
  }

  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
}
}
