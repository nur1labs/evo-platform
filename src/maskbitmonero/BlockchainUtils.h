#pragma once

#include <vector>

#include "core/trans/CachedTransaction.h"
#include "CryptoNote.h"
#include "base/CryptoNoteTools.h"

namespace CryptoNote {
namespace Utils {

bool restoreCachedTransactions(const std::vector<BinaryArray>& binaryTransactions, std::vector<CachedTransaction>& transactions);

} //namespace Utils
} //namespace CryptoNote
