#include "BlockchainStorage.h"

#include "MemoryBlockchainStorage.h"
#include "SwappedBlockchainStorage.h"

using namespace CryptoNote;

BlockchainStorage::BlockchainStorage(uint32_t reserveSize) : 
internalStorage(new MemoryBlockchainStorage(reserveSize)) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
BlockchainStorage::BlockchainStorage(const std::string& indexFileName, const std::string& dataFileName) : 
internalStorage(new SwappedBlockchainStorage(indexFileName, dataFileName)) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
BlockchainStorage::BlockchainStorage(std::unique_ptr<IBlockchainStorageInternal> storage) :
internalStorage(std::move(storage)) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
BlockchainStorage::~BlockchainStorage() {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void BlockchainStorage::pushBlock(RawBlock&& rawBlock) {
  internalStorage->pushBlock(std::move(rawBlock));
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
RawBlock BlockchainStorage::getBlockByIndex(uint32_t index) const {
  return internalStorage->getBlockByIndex(index);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
uint32_t BlockchainStorage::getBlockCount() const {
  return internalStorage->getBlockCount();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
//Returns MemoryBlockchainStorage with elements from [splitIndex, blocks.size() - 1].
//Original MemoryBlockchainStorage will contain elements from [0, splitIndex - 1].
std::unique_ptr<BlockchainStorage> BlockchainStorage::splitStorage(uint32_t splitIndex) {
  std::unique_ptr<BlockchainStorage> newStorage(new BlockchainStorage(internalStorage->splitStorage(splitIndex)));
  return newStorage;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
