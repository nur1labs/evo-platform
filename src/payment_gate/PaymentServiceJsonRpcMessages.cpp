#include "PaymentServiceJsonRpcMessages.h"
#include "Serialization/SerializationOverloads.h"

namespace PaymentService {

void Reset::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(viewSecretKey, "viewSecretKey");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void Reset::Response::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetViewKey::Request::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetViewKey::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(viewSecretKey, "viewSecretKey");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetStatus::Request::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetStatus::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(blockCount, "blockCount");
  serializer(knownBlockCount, "knownBlockCount");
  serializer(lastBlockHash, "lastBlockHash");
  serializer(peerCount, "peerCount");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void ValidateAddress::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(address, "address");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void ValidateAddress::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(isvalid, "isvalid");
  serializer(address, "address");
  serializer(spendPublicKey, "spendPublicKey");
  serializer(viewPublicKey, "viewPublicKey");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetAddresses::Request::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetAddresses::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(addresses, "addresses");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void CreateAddress::Request::serialize(CryptoNote::ISerializer& serializer) {
  bool hasSecretKey = serializer(spendSecretKey, "spendSecretKey");
  bool hasPublicKey = serializer(spendPublicKey, "spendPublicKey");
  if (!serializer(reset, "reset"))
     reset = true;
  if (hasSecretKey && hasPublicKey) {
    //TODO: replace it with error codes
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void CreateAddress::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(address, "address");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void DeleteAddress::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(address, "address")) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void DeleteAddress::Response::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetSpendKeys::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(address, "address")) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetSpendKeys::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(spendSecretKey, "spendSecretKey");
  serializer(spendPublicKey, "spendPublicKey");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetBalance::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(address, "address");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetBalance::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(availableBalance, "availableBalance");
  serializer(lockedAmount, "lockedAmount");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetBlockHashes::Request::serialize(CryptoNote::ISerializer& serializer) {
  bool r = serializer(firstBlockIndex, "firstBlockIndex");
  r &= serializer(blockCount, "blockCount");

  if (!r) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetBlockHashes::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(blockHashes, "blockHashes");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void TransactionHashesInBlockRpcInfo::serialize(CryptoNote::ISerializer& serializer) {
  serializer(blockHash, "blockHash");
  serializer(transactionHashes, "transactionHashes");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetTransactionHashes::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(addresses, "addresses");

  if (serializer(blockHash, "blockHash") == serializer(firstBlockIndex, "firstBlockIndex")) {
    throw RequestSerializationError();
  }

  if (!serializer(blockCount, "blockCount")) {
    throw RequestSerializationError();
  }

  serializer(paymentId, "paymentId");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetTransactionHashes::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(items, "items");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void TransferRpcInfo::serialize(CryptoNote::ISerializer& serializer) {
  serializer(type, "type");
  serializer(address, "address");
  serializer(amount, "amount");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void TransactionRpcInfo::serialize(CryptoNote::ISerializer& serializer) {
  serializer(state, "state");
  serializer(transactionHash, "transactionHash");
  serializer(blockIndex, "blockIndex");
  serializer(confirmations, "confirmations");
  serializer(timestamp, "timestamp");
  serializer(isBase, "isBase");
  serializer(unlockTime, "unlockTime");
  serializer(amount, "amount");
  serializer(fee, "fee");
  serializer(transfers, "transfers");
  serializer(extra, "extra");
  serializer(paymentId, "paymentId");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetTransaction::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(transactionHash, "transactionHash")) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetTransaction::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(transaction, "transaction");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void TransactionsInBlockRpcInfo::serialize(CryptoNote::ISerializer& serializer) {
  serializer(blockHash, "blockHash");
  serializer(transactions, "transactions");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetTransactions::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(addresses, "addresses");

  if (serializer(blockHash, "blockHash") == serializer(firstBlockIndex, "firstBlockIndex")) {
    throw RequestSerializationError();
  }

  if (!serializer(blockCount, "blockCount")) {
    throw RequestSerializationError();
  }

  serializer(paymentId, "paymentId");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetTransactions::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(items, "items");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetUnconfirmedTransactionHashes::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(addresses, "addresses");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetUnconfirmedTransactionHashes::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(transactionHashes, "transactionHashes");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletRpcOrder::serialize(CryptoNote::ISerializer& serializer) {
  bool r = serializer(address, "address");
  r &= serializer(amount, "amount");

  if (!r) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void SendTransaction::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(sourceAddresses, "addresses");

  if (!serializer(transfers, "transfers")) {
    throw RequestSerializationError();
  }

  serializer(changeAddress, "changeAddress");

  if (!serializer(fee, "fee")) {
    throw RequestSerializationError();
  }

  if (!serializer(anonymity, "anonymity")) {
    throw RequestSerializationError();
  }

  bool hasExtra = serializer(extra, "extra");
  bool hasPaymentId = serializer(paymentId, "paymentId");

  if (hasExtra && hasPaymentId) {
    throw RequestSerializationError();
  }

  serializer(unlockTime, "unlockTime");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void SendTransaction::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(transactionHash, "transactionHash");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void CreateDelayedTransaction::Request::serialize(CryptoNote::ISerializer& serializer) {
  serializer(addresses, "addresses");

  if (!serializer(transfers, "transfers")) {
    throw RequestSerializationError();
  }

  serializer(changeAddress, "changeAddress");

  if (!serializer(fee, "fee")) {
    throw RequestSerializationError();
  }

  if (!serializer(anonymity, "anonymity")) {
    throw RequestSerializationError();
  }

  bool hasExtra = serializer(extra, "extra");
  bool hasPaymentId = serializer(paymentId, "paymentId");

  if (hasExtra && hasPaymentId) {
    throw RequestSerializationError();
  }

  serializer(unlockTime, "unlockTime");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void CreateDelayedTransaction::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(transactionHash, "transactionHash");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetDelayedTransactionHashes::Request::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void GetDelayedTransactionHashes::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(transactionHashes, "transactionHashes");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void DeleteDelayedTransaction::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(transactionHash, "transactionHash")) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void DeleteDelayedTransaction::Response::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void SendDelayedTransaction::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(transactionHash, "transactionHash")) {
    throw RequestSerializationError();
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void SendDelayedTransaction::Response::serialize(CryptoNote::ISerializer& serializer) {
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void SendFusionTransaction::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(threshold, "threshold")) {
    throw RequestSerializationError();
  }

  if (!serializer(anonymity, "anonymity")) {
    throw RequestSerializationError();
  }

  serializer(addresses, "addresses");
  serializer(destinationAddress, "destinationAddress");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void SendFusionTransaction::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(transactionHash, "transactionHash");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void EstimateFusion::Request::serialize(CryptoNote::ISerializer& serializer) {
  if (!serializer(threshold, "threshold")) {
    throw RequestSerializationError();
  }

  serializer(addresses, "addresses");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void EstimateFusion::Response::serialize(CryptoNote::ISerializer& serializer) {
  serializer(fusionReadyCount, "fusionReadyCount");
  serializer(totalOutputCount, "totalOutputCount");
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
}
