#include "P2pContextOwner.h"
#include <cassert>
#include "P2pContext.h"

namespace CryptoNote {

P2pContextOwner::P2pContextOwner(P2pContext* ctx, ContextList& contextList) : contextList(contextList) {
  contextIterator = contextList.insert(contextList.end(), ContextList::value_type(ctx));
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
P2pContextOwner::P2pContextOwner(P2pContextOwner&& other) : contextList(other.contextList), contextIterator(other.contextIterator) {
  other.contextIterator = contextList.end();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
P2pContextOwner::~P2pContextOwner() {
  if (contextIterator != contextList.end()) {
    contextList.erase(contextIterator);
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
P2pContext& P2pContextOwner::get() {
  assert(contextIterator != contextList.end());
  return *contextIterator->get();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
P2pContext* P2pContextOwner::operator -> () {
  return &get();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
}
