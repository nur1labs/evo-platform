#include "WalletUnconfirmedTransactions.h"
#include "WalletLegacySerialization.h"

#include "base/CryptoNoteTools.h"
#include "ISerializer.h"
#include "Serialization/SerializationOverloads.h"

using namespace Crypto;

namespace CryptoNote {

inline TransactionOutputId getOutputId(const TransactionOutputInformation& out) {
  return std::make_pair(out.transactionPublicKey, out.outputInTransaction);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
WalletUnconfirmedTransactions::WalletUnconfirmedTransactions(uint64_t uncofirmedTransactionsLiveTime):
  m_uncofirmedTransactionsLiveTime(uncofirmedTransactionsLiveTime) {

}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::serialize(ISerializer& s) {
  s(m_unconfirmedTxs, "transactions");
  s(m_createdDeposits, "unconfirmedCreatedDeposits");
  s(m_spentDeposits, "unconfirmedSpentDeposits");

  if (s.type() == ISerializer::INPUT) {
    collectUsedOutputs();
  }

  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::deserializeV1(ISerializer& s) {
  s(m_unconfirmedTxs, "transactions");

  if (s.type() == ISerializer::INPUT) {
    collectUsedOutputs();
  }

  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::findTransactionId(const Hash& hash, TransactionId& id) {
  return findUnconfirmedTransactionId(hash, id) || findUnconfirmedDepositSpendingTransactionId(hash, id);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::findUnconfirmedTransactionId(const Crypto::Hash& hash, TransactionId& id) {
  auto it = m_unconfirmedTxs.find(hash);
  if (it == m_unconfirmedTxs.end()) {
    return false;
  }

  id = it->second.transactionId;
  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::findUnconfirmedDepositSpendingTransactionId(const Crypto::Hash& hash, TransactionId& id) {
  auto it = m_spentDeposits.find(hash);
  if (it == m_spentDeposits.end()) {
    return false;
  }

  id = it->second.transactionId;
  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::erase(const Hash& hash) {
  eraseUnconfirmedTransaction(hash) || eraseDepositSpendingTransaction(hash);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::eraseUnconfirmedTransaction(const Crypto::Hash& hash) {
  auto it = m_unconfirmedTxs.find(hash);
  if (it == m_unconfirmedTxs.end()) {
    return false;
  }

  deleteUsedOutputs(it->second.usedOutputs);
  m_unconfirmedTxs.erase(it);

  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::eraseDepositSpendingTransaction(const Crypto::Hash& hash) {
  auto it = m_spentDeposits.find(hash);
  if (it == m_spentDeposits.end()) {
    return false;
  }

  m_spentDeposits.erase(it);

  return true;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::add(const Transaction& tx, TransactionId transactionId, 
  uint64_t amount, const std::vector<TransactionOutputInformation>& usedOutputs) {

  UnconfirmedTransferDetails& utd = m_unconfirmedTxs[getObjectHash(tx)];

  utd.amount = amount;
  utd.sentTime = time(nullptr);
  utd.tx = tx;
  utd.transactionId = transactionId;

  uint64_t outsAmount = 0;
  // process used outputs
  utd.usedOutputs.reserve(usedOutputs.size());
  for (const auto& out : usedOutputs) {
    auto id = getOutputId(out);
    utd.usedOutputs.push_back(id);
    m_usedOutputs.insert(id);
    outsAmount += out.amount;
  }

  utd.outsAmount = outsAmount;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::updateTransactionId(const Hash& hash, TransactionId id) {
  auto it = m_unconfirmedTxs.find(hash);
  if (it != m_unconfirmedTxs.end()) {
    it->second.transactionId = id;
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::addCreatedDeposit(DepositId id, uint64_t totalAmount) {
  m_createdDeposits[id] = totalAmount;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::addDepositSpendingTransaction(const Hash& transactionHash, const UnconfirmedSpentDepositDetails& details) {
  assert(m_spentDeposits.count(transactionHash) == 0);
  m_spentDeposits.emplace(transactionHash, details);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::eraseCreatedDeposit(DepositId id) {
  m_createdDeposits.erase(id);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
uint64_t WalletUnconfirmedTransactions::countCreatedDepositsSum() const {
  uint64_t sum = 0;

  for (const auto& kv: m_createdDeposits) {
    sum += kv.second;
  }

  return sum;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
uint64_t WalletUnconfirmedTransactions::countSpentDepositsProfit() const {
  uint64_t sum = 0;

  for (const auto& kv: m_spentDeposits) {
    sum += kv.second.depositsSum - kv.second.fee;
  }

  return sum;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
uint64_t WalletUnconfirmedTransactions::countSpentDepositsTotalAmount() const {
  uint64_t sum = 0;

  for (const auto& kv: m_spentDeposits) {
    sum += kv.second.depositsSum;
  }

  return sum;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
uint64_t WalletUnconfirmedTransactions::countUnconfirmedOutsAmount() const {
  uint64_t amount = 0;

  for (auto& utx: m_unconfirmedTxs)
    amount+= utx.second.outsAmount;

  return amount;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
uint64_t WalletUnconfirmedTransactions::countUnconfirmedTransactionsAmount() const {
  uint64_t amount = 0;

  for (auto& utx: m_unconfirmedTxs)
    amount+= utx.second.amount;

  return amount;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
bool WalletUnconfirmedTransactions::isUsed(const TransactionOutputInformation& out) const {
  return m_usedOutputs.find(getOutputId(out)) != m_usedOutputs.end();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::collectUsedOutputs() {
  UsedOutputsContainer used;
  for (const auto& kv : m_unconfirmedTxs) {
    used.insert(kv.second.usedOutputs.begin(), kv.second.usedOutputs.end());
  }
  m_usedOutputs = std::move(used);
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::reset() {
  m_unconfirmedTxs.clear();
  m_usedOutputs.clear();
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
void WalletUnconfirmedTransactions::deleteUsedOutputs(const std::vector<TransactionOutputId>& usedOutputs) {
  for (const auto& output: usedOutputs) {
    m_usedOutputs.erase(output);
  }
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
std::vector<TransactionId> WalletUnconfirmedTransactions::deleteOutdatedTransactions() {
  std::vector<TransactionId> deletedTransactions;

  uint64_t now = static_cast<uint64_t>(time(nullptr));
  assert(now >= m_uncofirmedTransactionsLiveTime);

  for (auto it = m_unconfirmedTxs.begin(); it != m_unconfirmedTxs.end();) {
    if (static_cast<uint64_t>(it->second.sentTime) <= now - m_uncofirmedTransactionsLiveTime) {
      deleteUsedOutputs(it->second.usedOutputs);
      deletedTransactions.push_back(it->second.transactionId);
      it = m_unconfirmedTxs.erase(it);
    } else {
      ++it;
    }
  }

  return deletedTransactions;
}
//------------------------------------------------------------- Seperator Code -------------------------------------------------------------//
} /* namespace CryptoNote */
