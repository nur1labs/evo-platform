#pragma once

namespace Evo {

#define P2P_DEFAULT_PORT                                                9090
#define RPC_DEFAULT_PORT                                                9095

const std::initializer_list<const char*> SEED_NODES = {
  "0.0.0.0:9090",
};

struct CheckpointData {
  uint32_t height;
  const char* blockId;
};

#ifdef __GNUC__
__attribute__((unused))
#endif

// You may add here other checkpoints using the following format:
// {<block height>, "<block hash>"},
const std::initializer_list<CheckpointData> CHECKPOINTS = {
  //{ 9, "07234bfef0ab9f5f602f468ecfcfeb20057ba4c299c86f94e72239ee951ff0da" },
  //{ 7, "723c48735906a804c2c7fa01f3282a1a9d06c5d004b870f136f920d6ddc5c8a6" }
};

const std::map<const uint32_t, const uint8_t> Version = {
	// {BlockIndex , Version}
	{ 1000000, 1 }
};

} // CryptoNote
